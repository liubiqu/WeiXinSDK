#weixinsdk
=========
   
##微信公开帐号接口

    public class WeiXinUrl : IHttpHandler
    {
        static string token = "token";
        static string AppId = "AppId";
        static string AppSecret = "AppSecret";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var signature = context.Request["signature"] ?? string.Empty;
            var timestamp = context.Request["timestamp"] ?? string.Empty;
            var nonce = context.Request["nonce"] ?? string.Empty;
            //var echostr = context.Request.QueryString["echostr"] ?? string.Empty;
            if (WeiXin.CheckSignature(signature, timestamp, nonce, token))
            {
                //context.Response.Write(echostr);
                var replyMsg = WeiXin.ReplyMsg().GetXML();
                context.Response.Write(replyMsg);
            }
            else
            {
                context.Response.Write("fuck you");
            }

        }

        static WeiXinUrl()
        {
            WeiXin.ConfigGlobalCredential(AppId, AppSecret);
            //设置AccessToken缓存存取方案
    		Loogn.WeiXinSDK.WeiXin.ConfigAccessTokenCache((credential) =>
			{
				//需要使用绝对过期时间，NoAbsoluteExpiration这个可能存在两小时后{"errcode":42001,"errmsg":"access_token expired"}
				HttpRuntime.Cache.Add("WXAccessToken", credential.access_token, null, DateTime.Now.AddSeconds(credential.expires_in - 60), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
			}, () =>
			{
				return HttpRuntime.Cache.Get("WXAccessToken") as string;
			});
            WeiXin.RegisterMsgHandler<RecTextMsg>(msg =>
            {
                return new ReplyTextMsg { Content = "你说：" + msg.Content };
            });

            WeiXin.RegisterEventHandler<EventAttendMsg>(msg =>
            {
                return new ReplyTextMsg { Content = "谢谢关注！" };
            });
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
    
- 具体可以查看示例项目     
    
## Contributing

1. 登录 <http://git.oschina.net>
2. Fork <http://git.oschina.net/liubiqu/WeiXinSDK>
3. 创建您的特性分支 (`git checkout -b my-new-feature`)
4. 提交您的改动 (`git commit -am 'Added some feature'`)
5. 将您的改动记录提交到远程 git 仓库 (`git push origin my-new-feature`)
6. 然后到 github 网站的该 git 远程仓库的 `my-new-feature` 分支下发起 Pull Request


## Remote Sync

与远程库 `http://git.oschina.net/liubiqu/WeiXinSDK.git` 保持同步，避免冲突的办法如下：

1. `cd <your_fork_dir>`
2. `git remote add upstream http://git.oschina.net/liubiqu/WeiXinSDK.git`
3. `git fetch upstream`
4. `git merge upstream/master`

后续同步更新只需运行第3步和第4步。