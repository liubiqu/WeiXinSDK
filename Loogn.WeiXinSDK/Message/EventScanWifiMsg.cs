﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Loogn.WeiXinSDK.Message
{

    /// <summary>
    /// 扫描wifi链接二维码
    /// </summary>
    public class EventScanWifiMsg : EventBaseMsg
    {
        /// <summary>
        /// 事件KEY值，qrscene_为前缀，后面为二维码的参数值scene_id
        /// </summary>
        public string PlaceId { get; set; }
        /// <summary>
        /// 二维码的ticket，可用来换取二维码图片
        /// </summary>
        public string DeviceNo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public override string Event
        {
            get { return "wifi"; }
        }
    }
}
