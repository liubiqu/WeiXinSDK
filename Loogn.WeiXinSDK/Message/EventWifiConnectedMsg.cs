﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Loogn.WeiXinSDK.Message
{
    /// <summary>
    /// 微信wifi链接：链接后下发推送通知
    /// </summary>
    public class EventWifiConnectedMsg : EventBaseMsg
    {
        /// <summary>
        /// 连网时间（整型）
        /// </summary>
        public int ConnectTime { get; set; }
        /// <summary>
        /// 系统保留字段，固定值
        /// </summary>
        public int ExpireTime { get; set; }
        /// <summary>
        /// 系统保留字段，固定值
        /// </summary>
        public string VendorId { get; set; }
        /// <summary>
        /// 门店ID，即shop_id
        /// </summary>
        public string ShopId { get; set; }
        /// <summary>
        /// 连网的设备无线mac地址，对应bssid
        /// </summary>
        public string DeviceNo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public override string Event
        {
            get { return "WifiConnected"; }
        }
    }
}
