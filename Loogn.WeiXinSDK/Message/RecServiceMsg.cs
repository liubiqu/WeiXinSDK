﻿
namespace Loogn.WeiXinSDK.Message
{
    /// <summary>
    /// 客服消息
    /// </summary>
    public class RecServiceMsg : RecBaseMsg
    {
        /// <summary>
        /// 
        /// </summary>
        public override string MsgType
        {
            get { return "transfer_customer_service"; }
        }
    }
}