﻿using System;
using System.Collections.Generic;

namespace Loogn.WeiXinSDK
{
    /// <summary>
    /// 凭据
    /// </summary>
    [Serializable]
    public class ClientCredential
    {
        public string access_token { get; set; }
        public string jsapi_ticket { get; set; }
        /// <summary>
        /// 过期秒数
        /// </summary>
        public int expires_in { get; set; }

        public ReturnCode error { get; set; }

        static string TokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}";
        static string JsapiTicketUrl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={0}&type=jsapi";

        internal static ClientCredential GetCredential(string appId, string appSecret)
        {
            ClientCredential cred = null;
            var json = Util.HttpGet2(string.Format(TokenUrl, appId, appSecret));
            if (json.IndexOf("errcode") >= 0)
            {
                cred = new ClientCredential();
                cred.error = Util.JsonTo<ReturnCode>(json);
            }
            else
            {
                cred = Util.JsonTo<ClientCredential>(json);

                //获取jsapi_ticket
                var jsonTicket = Util.HttpGet2(string.Format(JsapiTicketUrl, cred.access_token));
                var ticketRet = Util.JsonTo<Ticket>(jsonTicket);
                if (ticketRet != null)
                {
                    ticketRet.add_time = DateTime.Now;
                    cred.jsapi_ticket = ticketRet.ticket;
                }
            }
            return cred;
        }
    }
}
